resource "azurerm_kubernetes_cluster_node_pool" "node-pool" {
    name                  = var.node_pool_name
    kubernetes_cluster_id = data.azurerm_kubernetes_cluster.aks.id
    vnet_subnet_id        = data.azurerm_subnet.aks_subnet.id
    node_count            = var.node_count
    vm_size               = var.vm_size
    zones    = var.node_pool_availability_zones
    enable_auto_scaling   = var.node_pool_enable_auto_scaling
    orchestrator_version  = var.kubernetes_version
    tags                  = var.tags
}