# GENERAL
variable "resource_group_name" {
  type        = string
  description = "RG name in Azure (Uses prefix)"
  default = "rg"
}

variable "location" {
  type        = string
  description = "Resources location in Azure"
  default = "WestEurope"
}

# NETWORK
variable "vnet_name" {
  type        = string
  description = "VNET name in Azure (Uses prefix)"
  default = "vnet"
}

variable "aks_subnet_address_name" {
  type        = string
  description = "AKS Subnet Address Name (Uses prefix)"
  default = "aks"
}

# AKS
variable "cluster_name" {
  type        = string
  description = "AKS name in Azure (Uses prefix)"
  default = "aks"
}

## Node pools ##
variable "node_pool_name" {
  type        = string
  description = "The name of the node pool in aks"
  default = "node-pool1"
}

variable "node_pool_availability_zones" {
  type        = list(number)
  description = "The availability zones of the node pool in aks"
  default = [1]
}

variable "node_pool_enable_auto_scaling" {
  type        = bool
  description = "Whether or not auto scaling is enabled in aks in Azure"
  default = false
}

variable "kubernetes_version" {
  type        = string
  description = "Kubernetes version"
  default = "1.26.3"
}

variable "node_count" {
  type        = number
  description = "Number of AKS worker nodes"
  default = 2
}

variable "vm_size" {
  type        = string
  description = "VM size for nodes in Azure"
  default = "standard_b2s"
}

## EXTRAS ##
variable "create_nodepool_linked_namespace_and_service_account" {
  type        = bool
  description = "Whether or not te create a new namespace linked to the new node pool and to create a service account, role and rolebinding to restrict the service account to only access the newly created namespace"
  default = false
}

variable "namespace_name" {
  type        = string
  description = "Name of the namespace for the aks in Azure"
  default = "node-pool1"
}

# TAGS
variable "tags" {
  type = map(any)

  default = {
    provisioner = "terraform"
    project     = "devsecops"
    environment = "development"
  }
}
