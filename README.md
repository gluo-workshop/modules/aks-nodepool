# aks-nodepool
```hcl
module "aks-nodepool" {
    source = "gitlab.com/gluobe/aks-nodepool/azure"
    version = "0.1.0"

    # GENERAL
    resource_group_name = "rg"
    location = "WestEurope"

    # NETWORK
    vnet_name = "vnet"
    aks_subnet_address_name = "aks"

    # AKS
    cluster_name = "aks"
    ## Node pools ##
    node_pool_name = "node-pool1"
    node_pool_availability_zones = [1]
    node_pool_enable_auto_scaling = false
    kubernetes_version = "1.23.8"
    node_count = 2
    vm_size = "standard_d2ads_v5"
    ## EXTRAS ##
    create_nodepool_linked_namespace_and_service_account = true
    namespace_name = "node-pool1"

    # TAGS
    tags = {
        provisioner = "terraform"
        project     = "devsecops"
        environment = "development"
    }
}
```