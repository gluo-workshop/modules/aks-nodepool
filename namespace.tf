resource "kubernetes_namespace" "namespace" {
    count = var.create_nodepool_linked_namespace_and_service_account == true ? 1 : 0
    metadata {
        annotations = {
        "scheduler.alpha.kubernetes.io/node-selector" = "agentpool=${var.node_pool_name}"
        }

        name = var.namespace_name
    }
    depends_on = [
        azurerm_kubernetes_cluster_node_pool.node-pool
    ]
}

resource "kubernetes_namespace" "testing-namespace" {
    metadata {
        annotations = {
        "scheduler.alpha.kubernetes.io/node-selector" = "agentpool=${var.node_pool_name}"
        }

        name = "testing-${var.namespace_name}"
    }
    depends_on = [
        azurerm_kubernetes_cluster_node_pool.node-pool
    ]
}

resource "kubernetes_service_account_v1" "service-account" {
    count = var.create_nodepool_linked_namespace_and_service_account == true ? 1 : 0
    metadata {
        name = "${var.node_pool_name}-serviceaccount"
        namespace = var.namespace_name
    }
    secret {
        name = "${var.node_pool_name}-serviceaccount-token"
    }
    depends_on = [
        kubernetes_namespace.namespace
    ]
}

resource "kubernetes_secret_v1" "service-account-secret" {
  metadata {
        name = "${var.node_pool_name}-serviceaccount-token"  
        annotations = {
            "kubernetes.io/service-account.name" = "${var.node_pool_name}-serviceaccount"
        }
        namespace = var.namespace_name
  }

  type = "kubernetes.io/service-account-token"
  wait_for_service_account_token = true
  depends_on = [
        kubernetes_namespace.namespace,
        kubernetes_service_account_v1.service-account
    ]
}

resource "kubernetes_role_v1" "role" {
    count = var.create_nodepool_linked_namespace_and_service_account == true ? 1 : 0
    metadata {
        name = "${var.node_pool_name}-user-role"
        namespace = var.namespace_name
    }
    rule {
        api_groups     = ["*"]
        resources      = ["events", "deployments", "replicasets", "pods", "services", "ingresses", "secrets", "pods/log", "pods/attach", "networkpolicies"]
        verbs          = ["*"]
    }
    depends_on = [
        kubernetes_namespace.namespace
    ]
}

resource "kubernetes_role_binding_v1" "role-binding" {
    count = var.create_nodepool_linked_namespace_and_service_account == true ? 1 : 0
    metadata {
        name      = "${var.node_pool_name}-user-roleBinding"
        namespace = var.namespace_name
    }
    subject {
        kind      = "ServiceAccount"
        name      = "${var.node_pool_name}-serviceaccount"
        namespace = var.namespace_name
    }
    role_ref {
        api_group = "rbac.authorization.k8s.io"
        kind      = "Role"
        name      = "${var.node_pool_name}-user-role"
    }
    depends_on = [
        kubernetes_service_account_v1.service-account,
        kubernetes_role_v1.role
    ]
}


resource "kubernetes_role_v1" "testing-role" {
    metadata {
        name = "testing-${var.node_pool_name}-user-role"
        namespace = "testing-${var.namespace_name}"
    }
    rule {
        api_groups     = ["*"]
        resources      = ["pods", "pods/attach"]
        verbs          = ["*"]
    }
    depends_on = [
        kubernetes_namespace.testing-namespace
    ]
}

resource "kubernetes_role_binding_v1" "testing-role-binding" {
    metadata {
        name      = "testing-${var.node_pool_name}-user-roleBinding"
        namespace = "testing-${var.namespace_name}"
    }
    subject {
        kind      = "ServiceAccount"
        name      = "${var.node_pool_name}-serviceaccount"
        namespace = var.namespace_name
    }
    role_ref {
        api_group = "rbac.authorization.k8s.io"
        kind      = "Role"
        name      = "testing-${var.node_pool_name}-user-role"
    }
    depends_on = [
        kubernetes_service_account_v1.service-account,
        kubernetes_role_v1.testing-role
    ]
}
