data "azurerm_kubernetes_cluster" "aks" {
  name                = var.cluster_name
  resource_group_name = var.resource_group_name
}

data "azurerm_subnet" "aks_subnet" {
  name                 = var.aks_subnet_address_name
  virtual_network_name = var.vnet_name
  resource_group_name  = var.resource_group_name
}